package com.vickie.anroidem2;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;

public class AppManagement {
	public static Queue<AppEntity> Queue1;
	public static Queue<AppEntity> Queue2;
	public static Queue<AppEntity> BusyService;
	public static int finishedapp;
	
	public static void InitAppManagement()
	{
		Queue1=new LinkedBlockingQueue<AppEntity>();
		Queue2=new LinkedBlockingQueue<AppEntity>();
		BusyService=new LinkedBlockingQueue<AppEntity>();
		finishedapp=0;

		AppEntity App1;
		AppEntity App2;
		AppEntity App3;
		AppEntity App4;
		AppEntity App5;
		AppEntity App6;
		AppEntity App7;
		AppEntity App8;
		AppEntity App9;
		AppEntity App10;
		AppEntity App11;
		AppEntity App15;
		AppEntity App16;
		
		AppEntity App12;
		AppEntity App13;
		AppEntity App14;
		
		if(MainActivity.energysaving==1)
		{
			App1=new AppEntity("pt.isec.tp.am", "com.vickie.freefalltest", Apptype.GAMEAPP, 1036800, "CPUAffinityService", 22);
			App2=new AppEntity("com.replica.replicaisland", "com.vickie.replicaislandtest", Apptype.GAMEAPP, 729600, "CPUAffinityService", 17.2);
			App3=new AppEntity("com.example.matrixapp1", Apptype.LATENCYAPP, 652800, "MatrixCalculate", 39900/20, 39900, 39900);
			App4=new AppEntity("com.example.matrixapp2", Apptype.LATENCYAPP, 960000, "MatrixCalculate", 20000/15, 20000, 20000);
			App5=new AppEntity("com.example.matrixapp3", Apptype.LATENCYAPP, 1036800, "MatrixCalculate", 12000/10, 12000, 12000);
			App6=new AppEntity("com.example.matrixapp4", Apptype.LATENCYAPP, 422400, "MatrixCalculate", 27000/8, 27000, 27000);
			App7=new AppEntity("com.example.matrixapp5", Apptype.LATENCYAPP, 729600, "MatrixCalculate", 22400/12, 22400, 22400);
			App8=new AppEntity("org.jtb.alogcat", "com.vickie.alogcattest", Apptype.UIAPP, 1190400, "CPUAffinityService", 100);
			App9=new AppEntity("si.modrajagoda.didi", "com.vickie.diditest", Apptype.UIAPP, 729600, "CPUAffinityService", 100);
			App10=new AppEntity("com.example.openglvideo", "com.vickie.openglvideotest", Apptype.VIDEOAPP, 729600, "CPUAffinityService", 4.5);
			App11=new AppEntity("com.vickie.videojohnoliver", "com.vickie.videojohntest", Apptype.VIDEOAPP, 729600, "CPUAffinityService", 4);
			App15=new AppEntity("com.aemobile.games.luckyfishing", "com.vickie.luckyfishingtest", Apptype.GAMEAPP, 1497600, "CPUAffinityService", 16);
			App16=new AppEntity("com.davemorrissey.labs.subscaleview.sample", "com.vickie.subscaleviewtest", Apptype.UIAPP, 1497600, "CPUAffinityService", 16);
			Log.d("AppManagement", "after init apps");
			init_Phase(App1);
			init_Phase(App2);
			init_Phase(App11);
			init_Phase(App15);
			Log.d("AppManagement", "after init phase");
		}
		else
		{
			App1=new AppEntity("pt.isec.tp.am", "com.vickie.freefalltest", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 22);
			App2=new AppEntity("com.replica.replicaisland", "com.vickie.replicaislandtest", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 17.2);
			App3=new AppEntity("com.example.matrixapp1", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 39900/20, 39900, 39900);
			App4=new AppEntity("com.example.matrixapp2", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 20000/15, 20000, 20000);
			App5=new AppEntity("com.example.matrixapp3", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 12000/10, 12000, 12000);
			App6=new AppEntity("com.example.matrixapp4", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 27000/8, 27000, 27000);
			App7=new AppEntity("com.example.matrixapp5", Apptype.LATENCYAPP, 1958400, "MatrixCalculate", 22400/12, 22400, 22400);
			App8=new AppEntity("org.jtb.alogcat", "com.vickie.alogcattest", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App9=new AppEntity("si.modrajagoda.didi", "com.vickie.diditest", Apptype.UIAPP, 1958400, "CPUAffinityService", 100);
			App10=new AppEntity("com.example.openglvideo", "com.vickie.openglvideotest", Apptype.VIDEOAPP, 1958400, "CPUAffinityService", 4.5);
			App11=new AppEntity("com.vickie.videojohnoliver", "com.vickie.videojohntest", Apptype.VIDEOAPP, 1958400, "CPUAffinityService", 4);
			App15=new AppEntity("com.aemobile.games.luckyfishing", "com.vickie.luckyfishingtest", Apptype.GAMEAPP, 1958400, "CPUAffinityService", 16);
			App16=new AppEntity("com.davemorrissey.labs.subscaleview.sample", "com.vickie.subscaleviewtest", Apptype.UIAPP, 1958400, "CPUAffinityService", 16);
		}
		
		if(MainActivity.phasecontrol==1||MainActivity.phasecontrol==2)
		{
			App1.multiple_phase=1;
			App11.multiple_phase=1;
			App2.multiple_phase=1;
			App15.multiple_phase=1;
		}
		
		App12=new AppEntity("com.example.busyservice1", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
		App13=new AppEntity("com.example.busyservice2", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
		App14=new AppEntity("com.example.busyservice3", Apptype.LATENCYAPP, 1497600, "MatrixCalculate", 0, 0, 0);
		
		Queue1.add(App9);
		Queue1.add(App1);
		Queue1.add(App2);
		Queue2.add(App3);
		Queue2.add(App4);
		Queue2.add(App5);
		Queue2.add(App6);
		Queue2.add(App7);
		Queue1.add(App8);
		Queue1.add(App10);
		Queue1.add(App11);
		BusyService.add(App12);
		BusyService.add(App13);
		BusyService.add(App14);
		
		Queue1.add(App15);
		Queue1.add(App16);
	}
	
	private static void init_Phase(AppEntity app)
	{
		if(app.AppName.equals("pt.isec.tp.am"))
		{
			app.trainingDataPath="freefall_trainingdata";
		app.phases=new Phase[2];
		app.phases[0]=new Phase();
		app.phases[1]=new Phase();
		
		app.phases[0].cpuUsage_max=0.08;
		app.phases[0].cpuUsage_min=0;
		app.phases[0].gpuUsage_max=0.000000000000000000000000001;
		app.phases[0].gpuUsage_min=0;
		app.phases[0].memUsage_max=0.1;
		app.phases[0].memUsage_min=0;
		app.phases[0].centerCPU=0.03754434;
		app.phases[0].centerGPU=0.090275716;
		app.phases[0].centerMEM=0.029726187;
		app.phases[0].freq=1036800;
		app.phases[0].sweetperformance=100;
		app.phases[0].AppType=Apptype.UIAPP;
		app.phases[0].count=0;
		app.phases[0].historyPerform=new double[]{0,0,0,0,0};
		
		app.phases[1].cpuUsage_max=0.25;
		app.phases[1].cpuUsage_min=0.1;
		app.phases[1].gpuUsage_max=0.3;
		app.phases[1].gpuUsage_min=0;
		app.phases[1].memUsage_max=0.1;
		app.phases[1].memUsage_min=0;
		app.phases[1].centerCPU=0.137827316;
		app.phases[1].centerGPU=0.124177423;
		app.phases[1].centerMEM=0.078029726;
		app.phases[1].freq=1267200;
		app.phases[1].sweetperformance=22;	
		app.phases[1].AppType=Apptype.GAMEAPP;
		app.phases[1].count=0;
		}
		else if(app.AppName.equals("com.vickie.videojohnoliver"))
		{
			app.trainingDataPath="video_trainingdata";
			app.phases=new Phase[3];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			app.phases[2]=new Phase();
			
			app.phases[0].cpuUsage_max=0.3;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0.000000000000000000000000001;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0.1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.198781277;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.033001503;
			app.phases[0].freq=1267200;
			app.phases[0].sweetperformance=100;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=0.3;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=0.9;
			app.phases[1].gpuUsage_min=0.5;
			app.phases[1].memUsage_max=0.1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.280574473;
			app.phases[1].centerGPU=0.80623069;
			app.phases[1].centerMEM=0.035312251;
			app.phases[1].freq=1036800;
			app.phases[1].sweetperformance=16;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
			
			app.phases[2].cpuUsage_max=0.3;
			app.phases[2].cpuUsage_min=0;
			app.phases[2].gpuUsage_max=0.5;
			app.phases[2].gpuUsage_min=0.000000000000000000000000001;
			app.phases[2].memUsage_max=0.1;
			app.phases[2].memUsage_min=0;
			app.phases[2].centerCPU=0.234110919;
			app.phases[2].centerGPU=0.382302033;
			app.phases[2].centerMEM=0.03740184;
			app.phases[2].freq=960000;
			app.phases[2].sweetperformance=4;
			app.phases[2].AppType=Apptype.VIDEOAPP;
			app.phases[2].count=0;
		}else if(app.AppName.equals("com.replica.replicaisland"))
		{
			app.trainingDataPath="replicaisland_trainingdata";
			app.phases=new Phase[3];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			app.phases[2]=new Phase();
			
			app.phases[0].cpuUsage_max=0.005;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=0.000000000000000000000000001;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=0.1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.015638413;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.034883292;
			app.phases[0].freq=729600;
			app.phases[0].sweetperformance=100;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=0.35;
			app.phases[1].cpuUsage_min=0.005;
			app.phases[1].gpuUsage_max=0.48;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=0.1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.153160193;
			app.phases[1].centerGPU=0.299663879;
			app.phases[1].centerMEM=0.03178318;
			app.phases[1].freq=1190400;
			app.phases[1].sweetperformance=1500;
			app.phases[1].AppType=Apptype.UIAPP;
			app.phases[1].count=0;
			app.phases[1].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[2].cpuUsage_max=0.15;
			app.phases[2].cpuUsage_min=0.005;
			app.phases[2].gpuUsage_max=0.8;
			app.phases[2].gpuUsage_min=0.48;
			app.phases[2].memUsage_max=0.1;
			app.phases[2].memUsage_min=0;
			app.phases[2].centerCPU=0.06508195;
			app.phases[2].centerGPU=0.52863194;
			app.phases[2].centerMEM=0.041065017;
			app.phases[2].freq=1036800;
			app.phases[2].sweetperformance=17.2;
			app.phases[2].AppType=Apptype.VIDEOAPP;
			app.phases[2].count=0;
		}else if(app.AppName.equals("com.aemobile.games.luckyfishing")) //training data should be updated before test
		{
			app.trainingDataPath="luckyfishing_trainingdata";
			app.phases=new Phase[2];
			app.phases[0]=new Phase();
			app.phases[1]=new Phase();
			
			app.phases[0].cpuUsage_max=1;
			app.phases[0].cpuUsage_min=0;
			app.phases[0].gpuUsage_max=1;
			app.phases[0].gpuUsage_min=0;
			app.phases[0].memUsage_max=1;
			app.phases[0].memUsage_min=0;
			app.phases[0].centerCPU=0.0327;
			app.phases[0].centerGPU=0;
			app.phases[0].centerMEM=0.0518;
			app.phases[0].freq=1190400;
			app.phases[0].sweetperformance=16;
			app.phases[0].AppType=Apptype.UIAPP;
			app.phases[0].count=0;
			app.phases[0].historyPerform=new double[]{0,0,0,0,0};
			
			app.phases[1].cpuUsage_max=1;
			app.phases[1].cpuUsage_min=0;
			app.phases[1].gpuUsage_max=1;
			app.phases[1].gpuUsage_min=0;
			app.phases[1].memUsage_max=1;
			app.phases[1].memUsage_min=0;
			app.phases[1].centerCPU=0.0801;
			app.phases[1].centerGPU=0.4361;
			app.phases[1].centerMEM=0.0542;
			app.phases[1].freq=1190400;
			app.phases[1].sweetperformance=16;
			app.phases[1].AppType=Apptype.VIDEOAPP;
			app.phases[1].count=0;
			app.phases[1].historyPerform=new double[]{0,0,0,0,0};
		}
	}
	
}
